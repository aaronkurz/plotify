# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Cdt(models.Model):
    device = models.ForeignKey('Ddt', models.DO_NOTHING, db_column='device', blank=True, null=True, related_name='Ddt_device')
    signal_variable = models.ForeignKey('Vdt', models.DO_NOTHING, db_column='signal_variable', related_name='Vdt_signal_variable')
    output_variable = models.ForeignKey('Vdt', models.DO_NOTHING, db_column='output_variable', related_name='Vdt_output_variable')
    experiment_time = models.DateTimeField(blank=True, null=True)
    reference_device = models.ForeignKey('Ddt', models.DO_NOTHING, db_column='reference_device', blank=True, null=True)
    method = models.CharField(max_length=32)
    param = models.CharField(max_length=256, blank=True, null=True)
    table = models.CharField(max_length=32, blank=True, null=True)
    tab_method = models.CharField(max_length=32, blank=True, null=True)
    idl_func = models.CharField(db_column='IDL_func', max_length=32, blank=True, null=True)  # Field name made lowercase.
    signal_clip_low = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    signal_clip_high = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    signal_clip_method = models.CharField(max_length=16, blank=True, null=True)
    value_clip_low = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    value_clip_high = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    value_clip_method = models.CharField(max_length=16, blank=True, null=True)
    next = models.ForeignKey('self', models.DO_NOTHING, db_column='next', blank=True, null=True)
    docu_directory = models.CharField(max_length=256, blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CDT'


class Ddt(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    serial_nr = models.CharField(max_length=32)
    model = models.CharField(max_length=64)
    manufacturer = models.CharField(max_length=64)
    comment = models.CharField(max_length=1024, blank=True, null=True)
    docu_directory = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DDT'
        unique_together = (('serial_nr', 'model', 'manufacturer'),)


class Edt(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    flag = models.CharField(max_length=16)
    mdt = models.ForeignKey('Mdt', models.DO_NOTHING, db_column='MDT_id')  # Field name made lowercase.
    comment = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EDT'
        unique_together = (('start_time', 'end_time', 'flag', 'mdt'),)


class Fdt(models.Model):
    path = models.CharField(primary_key=True, max_length=256)
    file_time_zone = models.CharField(max_length=8)
    format = models.CharField(max_length=64, blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    time_offset = models.IntegerField(blank=True, null=True)
    z_offset = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FDT'


class General(models.Model):
    name = models.CharField(db_column='NAME', primary_key=True, max_length=32)  # Field name made lowercase.
    version = models.CharField(db_column='VERSION', max_length=6, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'GENERAL'


class Hdt(models.Model):
    group = models.CharField(primary_key=True, max_length=256)
    mdt = models.OneToOneField('Mdt', models.DO_NOTHING, db_column='MDT_id')  # Field name made lowercase.
    overlap_priority = models.SmallIntegerField()
    comment = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HDT'
        unique_together = (('group', 'overlap_priority'), ('group', 'mdt'),)


class Idt(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    device = models.OneToOneField(Ddt, models.DO_NOTHING, db_column='device')
    site = models.ForeignKey('Sdt', models.DO_NOTHING, db_column='site')
    z_offset = models.DecimalField(max_digits=7, decimal_places=3)
    height_above_surface = models.DecimalField(max_digits=7, decimal_places=3)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True, null=True)
    x_offset = models.DecimalField(max_digits=8, decimal_places=3)
    y_offset = models.DecimalField(max_digits=8, decimal_places=3)
    terrain_height_difference = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    view_azimuth = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True)
    view_zenith = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True)
    rotation = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)
    indoor = models.SmallIntegerField(blank=True, null=True)
    project = models.CharField(max_length=64, blank=True, null=True)
    radiation_shield = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IDT'
        unique_together = (('device', 'site', 'start_time'),)


class Ldt(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    serial_nr = models.CharField(max_length=32)
    model = models.CharField(max_length=32)
    manufacturer = models.CharField(max_length=32)
    comment = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LDT'
        unique_together = (('serial_nr', 'model', 'manufacturer'),)


class Mdt(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    level = models.IntegerField()
    instrumentation = models.ForeignKey(Idt, models.DO_NOTHING, db_column='instrumentation')
    variable = models.ForeignKey('Vdt', models.DO_NOTHING, db_column='variable')
    read_file_info = models.ForeignKey('Rdt', models.DO_NOTHING, db_column='read_file_info', blank=True, null=True)
    nc_file = models.CharField(unique=True, max_length=256, blank=True, null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True, null=True)
    regular = models.SmallIntegerField(blank=True, null=True)
    agg_method = models.CharField(max_length=32, blank=True, null=True)
    n_samples = models.IntegerField(blank=True, null=True)
    min_n_valid = models.IntegerField(blank=True, null=True)
    interval = models.DecimalField(max_digits=11, decimal_places=3, blank=True, null=True)
    interval_unit = models.CharField(max_length=32, blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)
    calibration = models.ForeignKey(Cdt, models.DO_NOTHING, db_column='calibration', blank=True, null=True)
    source_mdt = models.ForeignKey('self', models.DO_NOTHING, db_column='source_MDT', blank=True, null=True)  # Field name made lowercase.
    featuretype = models.CharField(db_column='featureType', max_length=32)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MDT'
        unique_together = (('level', 'instrumentation', 'variable', 'read_file_info', 'start_time', 'end_time', 'agg_method', 'interval', 'interval_unit', 'calibration', 'source_mdt'),)


class Odt(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    mdt = models.ForeignKey(Mdt, models.DO_NOTHING, db_column='MDT_id')  # Field name made lowercase.
    file = models.OneToOneField(Fdt, models.DO_NOTHING, db_column='file')
    processed = models.IntegerField()
    added_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ODT'
        unique_together = (('file', 'mdt'),)


class Pdt(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    path = models.CharField(max_length=256, blank=True, null=True)
    logger = models.ForeignKey(Ldt, models.DO_NOTHING, db_column='logger')
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PDT'
        unique_together = (('path', 'logger', 'start_time'),)


class Rdt(models.Model):
    file_header_name = models.CharField(max_length=64)
    agg_method = models.CharField(max_length=32, blank=True, null=True)
    n_samples = models.IntegerField(blank=True, null=True)
    interval_sec = models.DecimalField(max_digits=11, decimal_places=3, blank=True, null=True)
    program = models.ForeignKey(Pdt, models.DO_NOTHING, db_column='program', blank=True, null=True)
    inconsistent_duplicate_action = models.CharField(max_length=32)
    consistent_duplicate_action = models.CharField(max_length=32)
    flag_inconsistent_duplicates = models.SmallIntegerField()
    flag_consistent_duplicates = models.SmallIntegerField()
    axis_rotate = models.CharField(max_length=64, blank=True, null=True)
    wind_partner_col = models.CharField(max_length=256, blank=True, null=True)
    file_fill_value = models.CharField(max_length=64, blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)
    time_interval_definition = models.CharField(max_length=32, blank=True, null=True)
    read_file_format = models.CharField(db_column='READ_FILE_FORMAT', max_length=64, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RDT'
        unique_together = (('file_header_name', 'program'),)


class Sdt(models.Model):
    code = models.CharField(primary_key=True, max_length=32)
    name = models.CharField(max_length=256, blank=True, null=True)
    manager = models.CharField(max_length=256)
    country = models.CharField(max_length=64, blank=True, null=True)
    city = models.CharField(max_length=64, blank=True, null=True)
    latitude = models.DecimalField(max_digits=26, decimal_places=10)
    longitude = models.DecimalField(max_digits=26, decimal_places=10)
    elevation = models.DecimalField(max_digits=8, decimal_places=4)
    horizon_nv = models.CharField(db_column='horizon_nV', max_length=256, blank=True, null=True)  # Field name made lowercase.
    horizon_v = models.CharField(db_column='horizon_V', max_length=256, blank=True, null=True)  # Field name made lowercase.
    transm_v = models.CharField(db_column='transm_V', max_length=256, blank=True, null=True)  # Field name made lowercase.
    svf_nv = models.DecimalField(db_column='SVF_nV', max_digits=5, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    svf_v = models.DecimalField(db_column='SVF_V', max_digits=5, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    svf = models.DecimalField(db_column='SVF', max_digits=5, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    lcz = models.CharField(db_column='LCZ', max_length=256, blank=True, null=True)  # Field name made lowercase.
    land_cover_type = models.CharField(max_length=256, blank=True, null=True)
    fractional_coverage = models.CharField(max_length=256, blank=True, null=True)
    areal_type = models.CharField(max_length=256, blank=True, null=True)
    docu_directory = models.CharField(max_length=256, blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SDT'
        unique_together = (('latitude', 'longitude'),)


class Vdt(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    long_name = models.CharField(max_length=128)
    standard_name = models.CharField(max_length=128, blank=True, null=True)
    unit = models.CharField(max_length=32)
    description = models.CharField(max_length=2096, blank=True, null=True)
    abbreviation = models.CharField(max_length=32)
    comment = models.CharField(max_length=1024, blank=True, null=True)
    source_long_name = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'VDT'
        unique_together = (('long_name', 'unit'),)


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class ExternalTool(models.Model):
    role = models.CharField(max_length=256)
    fdt_path = models.ForeignKey(Fdt, models.DO_NOTHING, db_column='fdt_path', blank=True, null=True)
    mdt = models.ForeignKey(Mdt, models.DO_NOTHING, blank=True, null=True)
    path = models.CharField(max_length=256, blank=True, null=True)
    value = models.CharField(max_length=256, blank=True, null=True)
    mechanism = models.CharField(max_length=256)
    group = models.CharField(max_length=256)
    comment = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'external_tool'
        unique_together = (('fdt_path', 'group', 'mechanism'), ('path', 'group', 'mechanism'), ('mdt', 'group', 'mechanism'),)

