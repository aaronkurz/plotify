<div id="Niederschlag"></div>

<script>
  var data = [
  {
    x: ['Mo', 'Di', 'Mi','DO','Fr'],
    y: [20, 14, 23, 18, 21, 10],
    type: 'bar'
  }
  ];

  var layout={
    title : 'Niederschlag'
  };

Plotly.newPlot('Niederschlag', data,layout);

</script>